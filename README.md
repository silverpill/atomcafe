# Atomcafe

Atomcafe is a simple [Atom](http://tools.ietf.org/html/rfc4287) feed generator.

- Uses CSS selectors to parse pages.
- Only works with server-side rendered pages.
- Does not require additional services, uses SQLite database.

## Installation

### Install from source

Atomcafe requires a stable version of [rust compiler](https://www.rust-lang.org/tools/install).

Compile using `cargo`:

```
cargo build --release
```

Run the compiled binary:

```
./target/release/atomcafe
```

Web application will be available at http://127.0.0.1:8080.

## Recipes

Recipe describes how to generate a feed from a web page. It is a YAML document containing several parameters:

- `name` - unique recipe name (required).
- `url` - page URL (required).
- `feed_title` - feed title (required).
- `post_selector` - post CSS selector (required).
- `post_title_selector` - post title CSS selector (required).
- `post_url_selector` - post URL CSS selector (required).
- `post_date_selector` - post date CSS selector (optional).
- `post_date_attribute` - post date attribute name (optional).
- `post_date_format` - post date format (optional).
- `post_date_regexp` - post date regular expression (optional).
- `post_author_selector` - post author CSS selector (optional).
- `post_summary_selector` - post summary CSS selector (optional).

Example:

```yaml
name: devto-week-top
url: https://dev.to/top/week
feed_title: dev.to Week Top
post_selector: .crayons-story__body
post_title_selector: .crayons-story__title a
post_url_selector: .crayons-story__title a
post_date_selector: .crayons-story__meta time
post_date_attribute: datetime
post_date_format: '%+'
post_author_selector: .crayons-story__meta .crayons-story__secondary
post_summary_selector: .crayons-story__tags
```

### Selectors

Almost all modern CSS selectors are supported.

`&` is a special selector that means reference to the post itself.

### Dates

If `post_date_selector` is not specified, current date is used.
If it is specified, then at least one of `post_date_format` and `post_date_regexp` must be present. `post_date_regexp` has a higher priority.

#### Format strings

Date format can defined using [date and time conversion specifiers](https://docs.rs/chrono/0.4.19/chrono/format/strftime/index.html).

Example: `%B %e, %Y`

If the year is missing, current year will be used.

#### Regular expressions

Regular expression for parsing date should contain capturing groups with special names. These names roughly correspond to date and time specifiers:

- `b`, `B`, `d`, `e`, `Y`

Example: `(?P<B>[a-zA-Z]+) (?P<e>\d{1,2})(?:st|nd|rd|th), (?P<Y>\d{4})`

In addition, some other capturing groups are supported:

- `ha` - captures the number of hours in strings like "8 hours ago".
- `ma` - captures the number of hours in strings like "20 minutes ago".

## Configuration

Atomcafe can be configured via environment variables or with `.env.local` file:

- `DATABASE_PATH` - path to SQLite database file.
- `HTTP_HOST` - IP address to serve on.
- `HTTP_PORT` - port to serve on.

## API

Update all feeds:

```
GET /feeds
```

Scheduler like cron can be used to call this method periodically.
