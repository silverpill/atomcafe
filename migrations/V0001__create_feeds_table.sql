CREATE TABLE IF NOT EXISTS feeds (
    recipe_name TEXT PRIMARY KEY,
    recipe TEXT NOT NULL, -- recipe as YAML document
    feed TEXT, -- feed data as JSON document
    generated_at TEXT,
    is_error BOOLEAN NOT NULL DEFAULT FALSE
);
