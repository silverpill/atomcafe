use regex::Regex;
use rusqlite::Error::QueryReturnedNoRows;
use serde::{Deserialize, Serialize};

use crate::database::Pool;
use crate::errors::HttpError;

#[derive(Deserialize, Serialize, Clone)]
pub struct Recipe {
    pub name: String,
    pub url: String,
    pub feed_title: String,
    pub post_selector: String,
    pub post_title_selector: String,
    pub post_url_selector: String,
    pub post_date_selector: Option<String>,
    pub post_date_attribute: Option<String>,
    pub post_date_format: Option<String>,
    pub post_date_regexp: Option<String>,
    pub post_author_selector: Option<String>,
    pub post_summary_selector: Option<String>,
}

pub fn get_recipes(db_pool: &Pool) -> Result<Vec<Recipe>, HttpError> {
    let conn = db_pool.get()?;
    let mut stmt = conn.prepare(
        "SELECT recipe FROM feeds ORDER BY recipe_name",
    )?;
    let result_iter = stmt.query_map(rusqlite::NO_PARAMS, |row| row.get(0))?;
    let mut recipe_list = Vec::new();
    for recipe_yaml_result in result_iter {
        let recipe_yaml: String = recipe_yaml_result?;
        let recipe = serde_yaml::from_str::<Recipe>(&recipe_yaml)?;
        recipe_list.push(recipe);
    }
    Ok(recipe_list)
}

pub fn get_recipe(
    db_pool: &Pool,
    recipe_name: &str,
) -> Result<Recipe, HttpError> {
    let conn = db_pool.get()?;
    let result = conn.query_row(
        "SELECT recipe FROM feeds WHERE recipe_name = ?1",
        rusqlite::params![recipe_name],
        |row| row.get::<_, String>(0),
    );
    let recipe_yaml = match result {
        Err(QueryReturnedNoRows) => {
            return Err(HttpError::NotFoundError("recipe".to_string()));
        },
        _ => result?,
    };
    let recipe = serde_yaml::from_str::<Recipe>(&recipe_yaml)?;
    Ok(recipe)
}

fn validate_recipe(recipe_yaml: &str) -> Result<Recipe, HttpError> {
    let recipe = serde_yaml::from_str::<Recipe>(recipe_yaml)
        .map_err(|_| HttpError::ValidationError("invalid recipe".into()))?;
    let recipe_name_re = Regex::new(r"[\w-]+").unwrap();
    if !recipe_name_re.is_match(&recipe.name) {
        let error_message = "invalid name";
        return Err(HttpError::ValidationError(error_message.into()));
    }
    if
        recipe.post_date_selector.is_some()
        && recipe.post_date_format.is_none()
        && recipe.post_date_regexp.is_none()
    {
        let error_message = "date format or regexp is required";
        return Err(HttpError::ValidationError(error_message.into()));
    }
    Ok(recipe)
}

#[derive(Deserialize)]
pub struct RecipeFormData {
    pub recipe_yaml: String,
}

pub fn create_or_update_recipe(
    db_pool: &Pool,
    recipe_yaml: &str,
) -> Result<(), HttpError> {
    let recipe = validate_recipe(&recipe_yaml)?;
    let conn = db_pool.get()?;
    conn.execute(
        "
        INSERT INTO feeds (recipe_name, recipe)
        VALUES (?1, ?2)
        ON CONFLICT (recipe_name)
        DO UPDATE SET recipe = ?2
        ",
        rusqlite::params![recipe.name, recipe_yaml],
    )?;
    Ok(())
}
