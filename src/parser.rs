use chrono::{DateTime, Duration, NaiveDate, NaiveDateTime, Utc};
use regex::Regex;
use scraper::{Html, ElementRef, Selector};
use ureq;
use url::Url;

use crate::feeds::{Feed, Entry};
use crate::recipes::Recipe;

struct WrappedSelector {
    css: String,
    selector: Option<Selector>,
}

impl WrappedSelector {
    fn parse(css: &str) -> Result<WrappedSelector, ParserError> {
        let selector = match css {
            "&" => None,
            _ => {
                // Selector errors are not exported
                // https://github.com/causal-agent/scraper/issues/60
                let selector = Selector::parse(css)
                    .map_err(|_| ParserError::SelectorError)?;
                Some(selector)
            },
        };
        Ok(WrappedSelector { css: css.clone().to_string(), selector })
    }

    fn select<'a>(&self, element: &ElementRef<'a>) -> Vec<ElementRef<'a>> {
        match &self.selector {
            Some(selector) => element.select(&selector).collect(),
            None => {
                // Self-reference
                assert_eq!(self.css, "&");
                vec![element.clone()]
            },
        }
    }

    fn select_one<'a>(&self, element: &ElementRef<'a>) -> Result<ElementRef<'a>, ParserError> {
        match self.select(element).get(0) {
            Some(el) => Ok(el.clone()),
            None => Err(ParserError::ElementNotFound(self.css.clone())),
        }
    }
}

fn get_min_datetime() -> DateTime<Utc> {
    DateTime::from_utc(NaiveDateTime::from_timestamp(0, 0), Utc)
}

fn get_text(element: ElementRef) -> String {
    let text: String = element.text().collect();
    text.trim().to_string()
}

fn urljoin(base: &str, path: &str) -> Result<String, url::ParseError> {
    let base_url = Url::parse(base)?;
    let url = base_url.join(path)?;
    Ok(url.to_string())
}

#[derive(thiserror::Error, Debug)]
pub enum DateParseError {
    #[error("date parsing error")]
    DateError(#[from] chrono::ParseError),

    #[error("regexp error")]
    RegexError(#[from] regex::Error),

    #[error("overflow error")]
    OverflowError,

    #[error("parsing error")]
    OtherError,
}

fn parse_date(
    date_str: &str,
    date_format: &str,
) -> Result<DateTime<Utc>, DateParseError> {
    if date_format == "%+" || date_format.contains("%H") {
        // Datetime
        let datetime = DateTime::parse_from_str(date_str, date_format)?;
        return Ok(datetime.with_timezone(&Utc));
    } else {
        // Date
        let date = if !date_format.contains("%Y") {
            // Year is missing
            let now: DateTime<Utc> = Utc::now();
            NaiveDate::parse_from_str(
                &format!("{} {}", date_str, now.format("%Y")),
                &format!("{} {}", date_format, "%Y"),
            )?
        } else {
            NaiveDate::parse_from_str(date_str, date_format)?
        };
        let datetime = date.and_hms(0, 0, 0);
        return Ok(DateTime::from_utc(datetime, Utc));
    }
}

fn parse_date_with_regexp(
    date_str: &str,
    date_regexp_str: &str,
) -> Result<DateTime<Utc>, DateParseError> {
    let date_regexp = Regex::new(date_regexp_str)?;
    let caps = date_regexp.captures(date_str)
        .ok_or(DateParseError::OtherError)?;
    // Check special groups
    for group_name in ["ha", "ma"] {
        if let Some(value_cap) = caps.name(group_name) {
            let value = value_cap.as_str().parse::<i64>()
                .map_err(|_| DateParseError::OtherError)?;
            let duration = match group_name {
                "ha" => Duration::hours(value),
                "ma" => Duration::minutes(value),
                _ => panic!("unexpected group name"),
            };
            let datetime = Utc::now()
                .checked_sub_signed(duration)
                .ok_or(DateParseError::OverflowError)?;
            return Ok(datetime);
        }
    }
    // Check datetime format specifiers
    let mut date_parts: Vec<String> = Vec::new();
    let mut date_fmt_parts: Vec<String> = Vec::new();
    for specifier in ["b", "B", "d", "e", "Y"] {
        if let Some(part) = caps.name(specifier) {
            date_parts.push(part.as_str().to_string());
            date_fmt_parts.push(format!("%{}", specifier));
        }
    }
    parse_date(
        &date_parts.join(" "),
        &date_fmt_parts.join(" "),
    )
}

#[derive(thiserror::Error, Debug)]
pub enum ParserError {
    #[error("http request error")]
    RequestError(#[from] ureq::Error),

    #[error("response parsing error")]
    ResponseError(#[from] std::io::Error),

    #[error("invalid selector")]
    SelectorError,

    #[error("url parsing error")]
    InvalidUrl(#[from] url::ParseError),

    #[error("date parsing error")]
    InvalidDate(#[from] DateParseError),

    #[error("element not found: {0}")]
    ElementNotFound(String),

    #[error("attribute not found")]
    AttributeNotFound,

    #[error("invalid recipe")]
    InvalidRecipe,
}

pub fn generate_feed(recipe: &Recipe) -> Result<Feed, ParserError> {
    let html = ureq::get(&recipe.url).call()?.into_string()?;
    let document = Html::parse_document(&html);
    let item_selector = WrappedSelector::parse(&recipe.post_selector)?;
    let title_selector = WrappedSelector::parse(&recipe.post_title_selector)?;
    let url_selector = WrappedSelector::parse(&recipe.post_url_selector)?;
    let date_selector = match &recipe.post_date_selector {
        Some(css) => Some(WrappedSelector::parse(css)?),
        None => None,
    };
    let author_selector = match &recipe.post_author_selector {
        Some(css) => Some(WrappedSelector::parse(css)?),
        None => None,
    };
    let summary_selector = match &recipe.post_summary_selector {
        Some(css) => Some(WrappedSelector::parse(css)?),
        None => None,
    };
    let mut entries: Vec<Entry> = Vec::new();
    let mut feed_updated_at = get_min_datetime();

    for element in item_selector.select(&document.root_element()) {
        let title_elem = title_selector.select_one(&element)?;
        let title = get_text(title_elem);
        let url_elem = url_selector.select_one(&element)?;
        let mut url = match url_elem.value().attr("href") {
            Some(value) => value.to_string(),
            None => return Err(ParserError::AttributeNotFound),
        };
        if url.starts_with("/") {
            url = urljoin(&recipe.url, &url)?;
        }
        let updated_at = match date_selector {
            Some(ref selector) => {
                let date_elem = selector.select_one(&element)?;
                let date_str = match recipe.post_date_attribute {
                    None => get_text(date_elem),
                    Some(ref attribute) => match date_elem.value().attr(attribute) {
                        Some(value) => value.to_string(),
                        None => return Err(ParserError::AttributeNotFound),
                    },
                };
                if let Some(post_date_regexp) = &recipe.post_date_regexp {
                    parse_date_with_regexp(&date_str, post_date_regexp)?
                } else if let Some(post_date_format) = &recipe.post_date_format {
                    parse_date(&date_str, post_date_format)?
                } else {
                    return Err(ParserError::InvalidRecipe);
                }
            },
            None => Utc::now(),
        };
        let author = match author_selector {
            Some(ref selector) => {
                let author_elem = selector.select_one(&element)?;
                Some(get_text(author_elem))
            },
            None => None,
        };
        let summary = match summary_selector {
            Some(ref selector) => {
                let summary_elem = selector.select_one(&element)?;
                Some(get_text(summary_elem))
            },
            None => None,
        };
        let entry = Entry {
            title: title,
            url: url,
            date: updated_at,
            author: author,
            summary: summary,
        };
        entries.push(entry);
        if updated_at > feed_updated_at {
            feed_updated_at = updated_at;
        }
    }
    if entries.len() == 0 {
        return Err(ParserError::ElementNotFound(item_selector.css));
    }
    let feed = Feed {
        title: recipe.feed_title.clone(),
        site_url: recipe.url.clone(),
        entries: entries,
        updated_at: feed_updated_at,
    };
    Ok(feed)
}
