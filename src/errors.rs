use actix_web::{
    dev::HttpResponseBuilder,
    http::header,
    http::StatusCode,
    HttpResponse,
    error::ResponseError,
};

#[derive(thiserror::Error, Debug)]
pub enum HttpError {
    #[error("json parsing error")]
    JsonError(#[from] serde_json::Error),

    #[error("yaml parsing error")]
    YamlError(#[from] serde_yaml::Error),

    #[error("date parsing error")]
    DateError(#[from] chrono::ParseError),

    #[error("database error")]
    DatabaseError(#[from] rusqlite::Error),

    #[error("database error")]
    DatabasePoolError(#[from] r2d2::Error),

    #[error("template rendering error")]
    TemplateError(#[from] askama::Error),

    #[error("validation error: {0}")]
    ValidationError(String),

    #[error("{0} not found")]
    NotFoundError(String),

    #[error("internal error")]
    InternalError,
}

impl ResponseError for HttpError {
    fn error_response(&self) -> HttpResponse {
        HttpResponseBuilder::new(self.status_code())
            .set_header(header::CONTENT_TYPE, "text/html; charset=utf-8")
            .body(self.to_string())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            HttpError::ValidationError(_) => StatusCode::BAD_REQUEST,
            HttpError::NotFoundError(_) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
