use actix_web::{
    get, post, web,
    App, HttpResponse, HttpServer,
    error::BlockingError,
    middleware::Logger as ActixLogger,
    http::header,
};
use askama::Template;
use r2d2_sqlite::SqliteConnectionManager;

mod config;
mod database;
mod errors;
mod feeds;
mod logger;
mod parser;
mod recipes;

use config::parse_config;
use database::Pool;
use database::migrate::apply_migrations;
use feeds::{
    Feed,
    save_feed,
    load_feed,
    set_feed_error,
    delete_feed,
    FeedInfo,
    get_feeds,
};
use logger::configure_logger;
use parser::generate_feed;
use recipes::{
    get_recipes,
    get_recipe,
    RecipeFormData,
    create_or_update_recipe,
};

#[derive(Template)]
#[template(path = "feed.xml")]
struct FeedTemplate {
    feed: Feed,
}

#[get("/feeds/{name}")]
async fn feed_handler(
    db_pool: web::Data<Pool>,
    web::Path(recipe_name): web::Path<String>,
) -> Result<HttpResponse, errors::HttpError> {
    let feed = load_feed(&db_pool, &recipe_name)?;
    let feed_template = FeedTemplate { feed: feed };
    let rendered = feed_template.render()?;
    let response = HttpResponse::Ok()
        .content_type("application/atom+xml")
        .body(rendered);
    Ok(response)
}

#[get("/feeds/{name}/generate")]
async fn generate_feed_handler(
    db_pool: web::Data<Pool>,
    web::Path(recipe_name): web::Path<String>,
) -> Result<HttpResponse, errors::HttpError> {
    let recipe = get_recipe(&db_pool, &recipe_name)?;
    let feed = match web::block(move || generate_feed(&recipe)).await {
        Ok(feed) => feed,
        Err(BlockingError::Error(parser_error)) => {
            let error_message = parser_error.to_string();
            set_feed_error(&db_pool, &recipe_name, &error_message)?;
            let response = HttpResponse::Ok().body(error_message);
            return Ok(response);
        },
        Err(BlockingError::Canceled) => {
            return Err(errors::HttpError::InternalError);
        },
    };
    save_feed(&db_pool, &recipe_name, &feed)?;
    let feed_template = FeedTemplate { feed: feed };
    let rendered = feed_template.render()?;
    let response = HttpResponse::Ok()
        .content_type("application/atom+xml")
        .body(rendered);
    Ok(response)
}

#[get("/feeds")]
async fn generate_all_handler(
    db_pool: web::Data<Pool>,
) -> Result<HttpResponse, errors::HttpError> {
    let recipe_list = get_recipes(&db_pool)?;
    let mut error_count = 0;
    for recipe in recipe_list {
        let recipe_copy = recipe.clone();
        match web::block(move || generate_feed(&recipe_copy)).await {
            Ok(feed) => {
                save_feed(&db_pool, &recipe.name, &feed)?;
            },
            Err(BlockingError::Error(parser_error)) => {
                let error_message = parser_error.to_string();
                set_feed_error(&db_pool, &recipe.name, &error_message)?;
                error_count += 1;
                log::warn!(
                    "failed to generate feed '{}': {}",
                    recipe.name, parser_error,
                );
            },
            Err(BlockingError::Canceled) => {
                return Err(errors::HttpError::InternalError);
            },
        };
    }
    Ok(HttpResponse::Ok().json(error_count))
}

#[derive(Template)]
#[template(path = "feed_list.html")]
struct FeedListTemplate {
    feed_list: Vec<FeedInfo>,
}

#[get("/")]
async fn feed_list_page_handler(
    db_pool: web::Data<Pool>,
) -> Result<HttpResponse, errors::HttpError> {
    let feed_list = get_feeds(&db_pool)?;
    let feed_list_template = FeedListTemplate { feed_list };
    let rendered = feed_list_template.render()?;
    let response = HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(rendered);
    Ok(response)
}

#[derive(Template)]
#[template(path = "delete_feed.html")]
struct DeleteFeedTemplate {
    recipe_name: String,
}

#[get("/feeds/{name}/delete")]
async fn delete_feed_page_handler(
    web::Path(recipe_name): web::Path<String>,
) -> Result<HttpResponse, errors::HttpError> {
    let template = DeleteFeedTemplate { recipe_name };
    let rendered = template.render()?;
    let response = HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(rendered);
    Ok(response)
}

#[post("/feeds/{name}/delete")]
async fn delete_feed_data_handler(
    db_pool: web::Data<Pool>,
    web::Path(recipe_name): web::Path<String>,
) -> Result<HttpResponse, errors::HttpError> {
    delete_feed(&db_pool, &recipe_name)?;
    let response = HttpResponse::Found()
        .header(header::LOCATION, "/").finish();
    Ok(response)
}

#[derive(Template)]
#[template(path = "import.html")]
struct RecipeImportTemplate { }

#[get("/import-recipe")]
async fn recipe_import_page_handler() -> Result<HttpResponse, errors::HttpError> {
    let recipe_import_template = RecipeImportTemplate { };
    let rendered = recipe_import_template.render()?;
    let response = HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(rendered);
    Ok(response)
}

#[post("/import-recipe")]
async fn recipe_import_data_handler(
    db_pool: web::Data<Pool>,
    form: web::Form<RecipeFormData>,
) -> Result<HttpResponse, errors::HttpError> {
    create_or_update_recipe(&db_pool, &form.recipe_yaml)?;
    let response = HttpResponse::Found()
        .header(header::LOCATION, "/").finish();
    Ok(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let conf = parse_config();
    configure_logger();
    let db_manager = SqliteConnectionManager::file(&conf.database_path);
    let db_pool = Pool::new(db_manager).expect("Failed to connect to database");
    apply_migrations(&conf.database_path);
    let listen_address = format!("{}:{}", conf.http_host, conf.http_port);
    HttpServer::new(move || {
        App::new()
            .wrap(ActixLogger::new("%r : %s : %a"))
            .data(db_pool.clone())
            .service(actix_files::Files::new("/static", "static"))
            .service(generate_feed_handler)
            .service(generate_all_handler)
            .service(feed_handler)
            .service(feed_list_page_handler)
            .service(delete_feed_page_handler)
            .service(delete_feed_data_handler)
            .service(recipe_import_page_handler)
            .service(recipe_import_data_handler)
    })
    .bind(listen_address)?
    .run()
    .await
}
