use dotenv;

#[derive(Clone)]
pub struct AppConfig {
    pub database_path: String,
    pub http_host: String,
    pub http_port: i16,
}

pub fn parse_config() -> AppConfig {
    dotenv::from_filename(".env.local").ok();
    dotenv::dotenv().ok();
    AppConfig {
        database_path: std::env::var("DATABASE_PATH")
            .expect("DATABASE_PATH is not set"),
        http_host: std::env::var("HTTP_HOST")
            .expect("HTTP_HOST is not set"),
        http_port: std::env::var("HTTP_PORT")
            .expect("HTTP_PORT is not set")
            .parse::<i16>()
            .expect("Invalid port number"),
    }
}
