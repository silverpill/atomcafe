pub mod migrate;
pub mod utils;

pub type Pool = r2d2::Pool<r2d2_sqlite::SqliteConnectionManager>;
