use rusqlite::Connection;

mod embedded {
    use refinery::embed_migrations;
    embed_migrations!("migrations");
}

pub fn apply_migrations(database_path: &str) {
    let mut conn = Connection::open(database_path).unwrap();
    let migration_report = embedded::migrations::runner()
        .run(&mut conn)
        .unwrap();

    for migration in migration_report.applied_migrations() {
        println!(
            "Migration Applied -  Name: {}, Version: {}",
            migration.name(),
            migration.version(),
        );
    }
}
