use chrono::{DateTime, NaiveDateTime, ParseError, Utc};

pub fn parse_sqlite_datetime(value: &str) -> Result<DateTime<Utc>, ParseError> {
    let datetime = NaiveDateTime::parse_from_str(&value, "%Y-%m-%d %H:%M:%S")?;
    Ok(DateTime::from_utc(datetime, Utc))
}
