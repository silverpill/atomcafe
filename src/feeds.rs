use chrono::{DateTime, Utc};
use rusqlite::Error::QueryReturnedNoRows;
use serde::{Deserialize, Serialize};

use crate::database::Pool;
use crate::database::utils::parse_sqlite_datetime;
use crate::recipes::Recipe;
use crate::errors::HttpError;

#[derive(Serialize, Deserialize)]
pub struct Entry {
    pub title: String,
    pub url: String,
    pub date: DateTime<Utc>,
    pub author: Option<String>,
    pub summary: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Feed {
    pub title: String,
    pub site_url: String,
    pub entries: Vec<Entry>,
    pub updated_at: DateTime<Utc>,
}

pub fn save_feed(db_pool: &Pool, recipe_name: &str, feed: &Feed) -> Result<(), HttpError> {
    let conn = db_pool.get()?;
    let feed_json = serde_json::to_string(feed)?;
    let updated_count = conn.execute(
        "
        UPDATE feeds
        SET
            feed = ?2,
            generated_at = datetime('now'),
            error_message = NULL
        WHERE recipe_name = ?1
        ",
        rusqlite::params![recipe_name, feed_json],
    )?;
    if updated_count == 0 {
        return Err(HttpError::NotFoundError("feed".to_string()));
    }
    Ok(())
}

pub fn load_feed(db_pool: &Pool, recipe_name: &str) -> Result<Feed, HttpError> {
    let conn = db_pool.get()?;
    let result = conn.query_row(
        "
        SELECT feed FROM feeds
        WHERE recipe_name = ?1 AND feed IS NOT NULL
        ",
        rusqlite::params![recipe_name],
        |row| row.get::<_, String>(0),
    );
    let feed_json = match result {
        Err(QueryReturnedNoRows) => {
            return Err(HttpError::NotFoundError("feed".to_string()));
        },
        _ => result?,
    };
    let feed = serde_json::from_str::<Feed>(&feed_json)?;
    Ok(feed)
}

pub fn set_feed_error(
    db_pool: &Pool,
    recipe_name: &str,
    error_message: &str,
) -> Result<(), HttpError> {
    let conn = db_pool.get()?;
    let updated_count = conn.execute(
        "UPDATE feeds SET error_message = ?2 WHERE recipe_name = ?1",
        rusqlite::params![recipe_name, error_message],
    )?;
    if updated_count == 0 {
        return Err(HttpError::NotFoundError("feed".to_string()));
    }
    Ok(())
}

pub fn delete_feed(
    db_pool: &Pool,
    recipe_name: &str,
) -> Result<(), HttpError> {
    let conn = db_pool.get()?;
    let deleted_count = conn.execute(
        "DELETE FROM feeds WHERE recipe_name = ?1",
        rusqlite::params![recipe_name],
    )?;
    if deleted_count == 0 {
        return Err(HttpError::NotFoundError("feed".to_string()));
    }
    Ok(())
}

pub struct FeedInfo {
    pub recipe: Recipe,
    pub recipe_pretty_yaml: String,
    pub generated_at: Option<DateTime<Utc>>,
    pub is_empty: bool,
    pub error_message: Option<String>,
}

pub fn get_feeds(db_pool: &Pool) -> Result<Vec<FeedInfo>, HttpError> {
    let conn = db_pool.get()?;
    let mut stmt = conn.prepare(
        "
        SELECT
            recipe,
            generated_at,
            feed IS NULL AS is_empty,
            error_message
        FROM feeds
        ORDER BY
            error_message IS NULL ASC,
            recipe_name ASC
        ",
    )?;
    let result_iter = stmt.query_map(rusqlite::NO_PARAMS, |row| {
        let result: (String, Option<String>, bool, Option<String>) = (
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
            row.get(3)?,
        );
        Ok(result)
    })?;
    let mut feed_list = Vec::new();
    for result in result_iter {
        let (recipe_yaml, generated_at_str, is_empty, error_message) = result?;
        let recipe = serde_yaml::from_str::<Recipe>(&recipe_yaml)?;
        let generated_at = match generated_at_str {
            Some(value) => Some(parse_sqlite_datetime(&value)?),
            None => None,
        };
        let feed_info = FeedInfo {
            recipe,
            recipe_pretty_yaml: recipe_yaml,
            generated_at,
            is_empty,
            error_message,
        };
        feed_list.push(feed_info);
    }
    Ok(feed_list)
}
